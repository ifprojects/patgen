export class Item {

    nomeProduto: String;
    categoria: String;
    dataCompra: Date;
    valor: number;
}