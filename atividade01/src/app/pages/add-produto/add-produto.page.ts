import { Component, OnInit, LOCALE_ID } from '@angular/core';
import { Item } from 'src/app/models/Item';
import { Navigation } from 'selenium-webdriver';
import { NavController } from '@ionic/angular';
import { ViewController } from '@ionic/core';
import { formatCurrency } from '@angular/common';
import localePtBr from '@angular/common/locales/pt';
import { registerLocaleData } from '@angular/common';
import { Utils } from 'src/app/utils/Utils';




@Component({
  selector: 'app-add-produto',
  templateUrl: './add-produto.page.html',
  styleUrls: ['./add-produto.page.scss'],
})
export class AddProdutoPage implements OnInit {


  private item : Item;

  constructor(public navController : NavController) { 
    this.item = new Item();
    registerLocaleData(localePtBr, 'pt-BR');
  }

  ngOnInit() {
  
  }

  adicionar(){
    var stringfiedItem = JSON.stringify({
      nomeProduto : this.item.nomeProduto,
      categoria : this.item.categoria,
      dataCompra : this.item.dataCompra,
      valor : formatCurrency(this.item.valor,'pt-BR','R$')
    });
    localStorage.setItem(this.item.nomeProduto.toString(),stringfiedItem);
  }
}
