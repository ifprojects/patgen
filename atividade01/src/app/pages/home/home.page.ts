import { Component } from '@angular/core';
import { Item } from 'src/app/models/Item';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  private items : Array<Item>;

  constructor() {
    this.items = new Array();
  }

  ngOnInit() {
    this.listaItems();

  }

  public listaItems(){
    for ( var i = 0, len = localStorage.length; i < len; ++i ) {
      this.items.push((JSON.parse(localStorage.getItem(localStorage.key(i)))));
    }
    console.log(this.items);
  }

}
