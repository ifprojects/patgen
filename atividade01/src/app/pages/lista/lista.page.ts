import { Component, OnInit } from '@angular/core';
import { Item } from 'src/app/models/Item';
import { TouchSequence } from 'selenium-webdriver';
import { NavController } from '@ionic/angular';
import { Utils } from 'src/app/utils/Utils';
import { error } from 'util';



@Component({
  selector: 'app-lista',
  templateUrl: './lista.page.html',
  styleUrls: ['./lista.page.scss'],

})
export class ListaPage implements OnInit {

  private items : Array<Item>;
  private item : Item;
  private option : any;

  constructor(public navController : NavController,  ) { 
    this.items = new Array();
    this.item = new Item();
  }

  ngOnInit() {
    this.listaNomeItems();

  }


  public listaNomeItems(){
    for ( var i = 0;i < localStorage.length ; ++i ) {
      this.items.push((JSON.parse(localStorage.getItem(localStorage.key(i)))));
    }
    console.log(this.items);
  }

  public loadItem(){
    if(this.item.nomeProduto == undefined){
      console.log("Erro");
      return error;
    }
    this.item = (JSON.parse(localStorage.getItem(this.option)));
  }

  public updateItem(){
    if(this.item.nomeProduto == undefined){
      console.log("Erro");
      return error;
    }
    var strinfiedItem = JSON.stringify(this.item);
    localStorage.setItem(this.option, strinfiedItem);
  }

  public deleteItem(){
    if(this.item.nomeProduto == undefined){
      return error;
    }
    localStorage.removeItem(this.item.nomeProduto.toString());
  }

}